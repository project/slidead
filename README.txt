README - SLIDE AD
**************************


SHORT PROJECT DESCRIPTION
--------------------------
Slide Ad is a simple module that allows show a little 
box in the bottom of page while user is scrolling.

What can I do?

* Show Social Media buttons
* Call to actions about a product or article
* Show Offers
* Show Ads
* Whatever!


REQUIREMENTS
------------
  - JavaScript enabled in your browser


INSTALLATION
------------
1. Place the slidead directory into your Drupal sites/all/modules/ directory.
2. You can configure it on:  admin/config/user-interface/slidead


TODO
-----
The box is only showed at the bottom right position, 
in the future you can configure the custom position.


AUTHOR
------
Braulio Soncco
User: soncco@drupal.org
Email: soncco@gmail.com
